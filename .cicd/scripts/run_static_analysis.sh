DJANGO_SETTINGS_MODULE=forum.settings \
pylint --load-plugins pylint_django --load-plugins pylint_django.checkers.migrations --rcfile=.cicd/config/pylint $@ && \
bandit -c=.cicd/config/bandit.config -r $@ && \
#mypy --show-error-codes --config-file .cicd/config/mypy.ini $@
echo "Static analysis finished"