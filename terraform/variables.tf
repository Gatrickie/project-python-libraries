variable "postgres_plan" {
  type        = string
  description = "PostgreSQL heroku plan"
  default     = "heroku-postgresql:hobby-dev"
}

variable "app_name" {
  type    = string
  default = "python-forum-raf-jer"
}

variable "app_path" {
  type    = string
  default = "../forum"
}

variable "secret_key" {
  type = string

}